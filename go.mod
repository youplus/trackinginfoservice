module bitbucket.org/youplus/trackinginfoservice

go 1.12

require (
	github.com/ghodss/yaml v1.0.0 // indirect
	github.com/gin-contrib/gzip v0.0.1
	github.com/gin-gonic/gin v1.4.0
	github.com/go-stack/stack v1.8.0 // indirect
	github.com/golang/snappy v0.0.1 // indirect
	github.com/jasonlvhit/gocron v0.0.0-20191111122648-5c21418a78e8
	github.com/sirupsen/logrus v1.4.2
	github.com/streadway/amqp v0.0.0-20190827072141-edfb9018d271
	github.com/tkanos/gonfig v0.0.0-20181112185242-896f3d81fadf
	github.com/xdg/scram v0.0.0-20180814205039-7eeb5667e42c // indirect
	github.com/xdg/stringprep v1.0.0 // indirect
	go.mongodb.org/mongo-driver v1.1.3
	gopkg.in/mgo.v2 v2.0.0-20190816093944-a6b53ec6cb22
)

replace github.com/ugorji/go v1.1.4 => github.com/ugorji/go/codec v0.0.0-20190204201341-e444a5086c43
