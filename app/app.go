package app

import (
	"io"
	"math/rand"
	"os"
	"time"

	"bitbucket.org/youplus/trackinginfoservice/app/infrastructure"
	"bitbucket.org/youplus/trackinginfoservice/app/usecase/analytics"
	"bitbucket.org/youplus/trackinginfoservice/app/utils"
	"github.com/gin-contrib/gzip"
	"github.com/gin-gonic/gin"
	"github.com/jasonlvhit/gocron"
	log "github.com/sirupsen/logrus"
)

var _logger = utils.GetLogger()

var configService = infrastructure.ConfigService{}

type App struct{}

var engagementService = analytics.EngagementService{}

func (app *App) Init() {

	rand.Seed(time.Now().UnixNano())
	gin.SetMode(gin.ReleaseMode)
	routes := gin.Default()
	routes.Use(gzip.Gzip(gzip.DefaultCompression))
	routes.Use(gin.ErrorLogger())
	routes.Use(gin.Recovery())

	analytics := routes.Group("engagement")
	{

		analytics.POST("/track", SaveTrackingInfo)
	}
	routes.StaticFile("/favicon.ico", "")
	routes.Run(":8000")
}

//InitLogger ... Inits the logger with the provided log level
func (app App) InitLogger(f *os.File) {
	cfg := configService.GetConfiguration()
	lvl, err := log.ParseLevel(cfg.LogLevel)
	if err != nil {
		_logger.Error("Couldn't read log level. Log level set to INFO as default.")
	} else {
		_logger.SetLevel(lvl)
	}
	// _logger.SetReportCaller(true)
	_logger.SetOutput(io.MultiWriter(os.Stdout, f))
}

//InitScheduler initializes the app with predefined configuration(schedular for updating SEC)
func (app App) InitScheduler() {
	gocron.Start()

	gocron.Every(1).Minutes().Do(func() {
		engagementService.ConsumeStatisticsFromRMQ()
	})
}

//SaveTrackingInfo ... To save all the tracking information
//Author: Mukesh / Manan
//Updated On: 30/01/2019
func SaveTrackingInfo(c *gin.Context) {

	res, status := engagementService.SaveTrackingInfo(c.PostForm("data"))
	c.Header("Access-Control-Allow-Origin", "*")
	c.SecureJSON(status, res)
}
