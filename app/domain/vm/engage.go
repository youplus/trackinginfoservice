package vm

//UserEngagementVM ... View model for user engagement
type UserEngagementVM struct {
	UniqueUserID   string `json:"uuid"`
	SessionID      string `json:"sid"`
	GroupID        int    `json:"gid"`
	WidgetID       string `json:"widget_id"`
	WidgetURL      string `json:"widget_url"`
	EventName      string `json:"event_name"`
	Label          string `json:"label"`
	QuestionID     string `json:"question_id"`
	VideoID        string `json:"video_id"`
	VideoType      string `json:"video_type"`
	StartTimestamp *int64 `json:"start_ts"` //Unix timestamp in seconds
	UserAgent      string `json:"user_agent"`
	WidgetStatus   string `json:"widget_status"`
	NewSession     bool   `json:"is_new_session"`
	IsMobile       bool   `json:"is_mobile"`
}
