package domain

import (
	"time"

	"gopkg.in/mgo.v2/bson"
)

//UserEngagement : base structure for user engagement
type UserEngagement struct {
	ID              bson.ObjectId `json:"_id" bson:"_id"`
	UniqueUserID    string        `json:"uuid" bson:"uuid"`
	SessionID       string        `json:"sid" bson:"sid"`
	GroupID         int           `json:"gid" bson:"gid"`
	WidgetID        string        `json:"widget_id" bson:"widget_id"`
	WidgetURL       string        `json:"widget_url" bson:"widget_url"`
	EventID         string        `json:"event_id" bson:"event_id"`
	EventName       string        `json:"event_name" bson:"event_name"`
	EventSequence   string        `json:"event_sequence" bson:"event_sequence"`
	Label           string        `json:"label" bson:"label"`
	QuestionID      string        `json:"question_id" bson:"question_id"`
	VideoID         string        `json:"video_id" bson:"video_id"`
	VideoType       string        `json:"video_type" bson:"video_type"`
	Domain          string        `json:"domain" bson:"domain"`
	StartTime       int64         `json:"start_time" bson:"start_time"`
	UserAgent       string        `json:"user_agent" bson:"user_agent"`
	ServerTimestamp string        `json:"server_ts" bson:"server_ts"`
	CreatedAt       time.Time     `json:"created_at" bson:"created_at"`
	UpdatedAt       time.Time     `json:"updated_at" bson:"updated_at"`
	WidgetStatus    string        `json:"widget_status" bson:"widget_status"`
	NewSession      bool          `json:"is_new_session"`
	IsMobile        bool          `json:"is_mobile" bson:"is_mobile"`
}

//TopPages : base structure for fetching top performing pages
type TopPages struct {
	ID         string  `json:"_id" bson:"_id"`
	AvgTime    float64 `json:"avg_time" bson:"avg_time"`
	ViewCount  float64 `json:"view_count" bson:"view_count"`
	TotalCount float64 `json:"total_count" bson:"total_count"`
}

//TopVideos : base structure for fetching top performing videos
type TopVideos struct {
	VideoID           string `json:"video_id" bson:"_id"`
	Title             string `json:"title" bson:"title"`
	Question          string `json:"question" bson:"question"`
	ThumbnailImageURL string `json:"thumbnail_image_url" bson:"thumbnail_image_url"`
	ViewCount         int64  `json:"views" bson:"views"`
	WidgetURL         string `json:"widget_url" bson:"widget_url"`
}

type TopQVMVideos struct {
	VideoID           bson.ObjectId `json:"video_id" bson:"_id"`
	Title             string        `json:"title" bson:"title"`
	Question          string        `json:"question" bson:"question"`
	ThumbnailImageURL string        `json:"thumbnail_image_url" bson:"thumbnail_image_url"`
}

//SiteStats : base structure for statistics of a website
type SiteStats struct {
	TotalPages    int32 `json:"total_pages" bson:"total_pages"`
	PlanningCount int32 `json:"planning_count" bson:"planning_count"`
	LiveCount     int32 `json:"live_count" bson:"live_count"`
	QueueCount    int32 `json:"queue_count" bson:"queue_count"`
	PauseCount    int32 `json:"paused_count" bson:"paused_count"`
}

//QTags : base structure for qTag collection
type QTags struct {
	VideoID           string `json:"video_id" bson:"video_id"`
	ThumbnailImageURL string `json:"thumbnail_image_url" bson:"thumbnail_image_url"`
	MpdQuestions      []struct {
		ID       bson.ObjectId `json:"_id" bson:"_id"`
		Question string        `json:"question" bson:"question"`
	} `json:"mpd_questions" bson:"mpd_questions"`
}

//LabelType Type of the label for an click event
type LabelType int32

const (
	//VIDEOPLAY label type
	VIDEOPLAY LabelType = iota
	//SHAREOPINION label type
	SHAREOPINION
	//SCROLLTRANSCRIPTION label type
	SCROLLTRANSCRIPTION
	//REPORTVIDEO label type
	REPORTVIDEO
	//MUTEVIDEO label type
	MUTEVIDEO
	//UNMUTEVIDEO label type
	UNMUTEVIDEO
	//PREVIOUSVIDEO label type
	PREVIOUSVIDEO
	//NEXTVIDEO label type
	NEXTVIDEO
	//REPLAYVIDEO label type
	REPLAYVIDEO
	//WEBSITEREDIRECT label type
	WEBSITEREDIRECT
	//FULLSCREEN label type
	FULLSCREEN

	//CTA label type
	CTA

	//PLAY play video label type
	PLAY

	//PAUSE pause video label type
	PAUSE

	//LABELEND ending label for validation
	LABELEND
)

//GetLabel ... Gets label from the label string passed
func GetLabel(str string) LabelType {
	switch str {
	case "VIDEOPLAY":
		return VIDEOPLAY
	case "SHAREOPINION":
		return SHAREOPINION
	case "SCROLLTRANSCRIPTION":
		return SCROLLTRANSCRIPTION
	case "REPORTVIDEO":
		return REPORTVIDEO
	case "MUTEVIDEO":
		return MUTEVIDEO
	case "UNMUTEVIDEO":
		return UNMUTEVIDEO
	case "PREVIOUSVIDEO":
		return PREVIOUSVIDEO
	case "NEXTVIDEO":
		return NEXTVIDEO
	case "REPLAYVIDEO":
		return REPLAYVIDEO
	case "WEBSITEREDIRECT":
		return WEBSITEREDIRECT
	case "FULLSCREEN":
		return FULLSCREEN
	case "CTA":
		return CTA
	case "PLAY":
		return PLAY
	case "PAUSE":
		return PAUSE
	default:
		return LABELEND
	}
}

//EventName Type of the event
type EventName int32

const (
	//REGUUID event name
	REGUUID EventName = iota
	//WIDGETLOAD event name
	WIDGETLOAD
	//CLICK event name
	CLICK
	//TABCHANGE event name
	TABCHANGE
	//BROWSERCLOSE event name
	BROWSERCLOSE
	//AUTOPLAY event name
	AUTOPLAY
	//MANUAL event name
	MANUAL

	//EVENTNAMEEND ending event name for validation
	EVENTNAMEEND

	//BOTLOAD event for bots
	BOTLOAD
)

//GetEventName ... Gets event name from the event name string passed
func GetEventName(str string) EventName {
	switch str {
	case "REGUUID":
		return REGUUID
	case "WIDGETLOAD":
		return WIDGETLOAD
	case "CLICK":
		return CLICK
	case "TABCHANGE":
		return TABCHANGE
	case "BROWSERCLOSE":
		return BROWSERCLOSE
	case "AUTOPLAY":
		return AUTOPLAY
	case "MANUAL":
		return MANUAL
	case "BOTLOAD":
		return BOTLOAD
	default:
		return EVENTNAMEEND
	}
}

// WidgetConfigs is the widget_configs model
type WidgetConfigs struct {
	ID             string    `json:"_id" bson:"_id"`
	Publisher      string    `json:"site_id" bson:"site_id"`
	ViewStatus     string    `json:"view_status" bson:"view_status"`
	ViewType       int       `json:"view_type" bson:"view_type"`
	Ecommerce      bool      `json:"is_ecom" bson:"is_ecom"`
	Category       string    `json:"category_id" bson:"category_id"`
	Count          int       `json:"count" bson:"count"`
	VideoOrders    []string  `json:"video_orders" bson:"video_orders"`
	Sessions       int32     `json:"sessions" bson:"sessions"`
	InitialSession time.Time `json:"initial_session" bson:"initial_session"`
}
