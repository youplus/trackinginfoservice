package utils

import (
	"fmt"
	"strings"
)

var _logger = GetLogger()

const (
	MSG_CONTENT_NOT_FOUND int = iota
	MSG_NO_MATCHING_RECORDS
	MSG_BAD_VALUE
	MSG_BAD_PARAMETERS
	MSG_UPDATE_SUCCESS
	MSG_UPDATE_FAILED
)

//GetErrResponse ... Returns a formatted json reponse containing a message and a success status.
func GetErrResponse(msg int, fields []string) map[string]interface{} {
	var message string
	success := false
	switch msg {
	case MSG_CONTENT_NOT_FOUND:
		message = "The requested content couldnot be found."
	case MSG_NO_MATCHING_RECORDS:
		message = "No matching records found for the provided criteria."
	case MSG_BAD_VALUE:
		message = "The following fields are bad values: " + strings.Join(fields, ",")
	case MSG_BAD_PARAMETERS:
		message = "Malformed request received. Please check the parameters."
	case MSG_UPDATE_SUCCESS:
		message = "Updated the records successfully."
		success = true
	case MSG_UPDATE_FAILED:
		message = "Failed to update the records."
	default:
		_logger.Error("Undefined error received.")
	}
	return map[string]interface{}{"message": message, "success": success}
}

type ErrorInternalServer struct{}

func (ErrorInternalServer) GetErrResponse() map[string]interface{} {
	return map[string]interface{}{"message": "Internal server error.", "success": false}
}

type ErrorOneof struct{}

func (ErrorOneof) GetErrResponse(field string, values []string) map[string]interface{} {
	return map[string]interface{}{"message": fmt.Sprintf("The '%s' can take only of the following values: "+strings.Join(values, ","), field), "success": false}
}

type ErrorSessionExpired struct{}

func (ErrorSessionExpired) GetErrResponse() map[string]interface{} {
	return map[string]interface{}{"message": "The requested session has expired.", "success": false}
}
