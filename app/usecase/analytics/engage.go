package analytics

import (
	"context"
	"encoding/json"
	"errors"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"time"

	"bitbucket.org/youplus/trackinginfoservice/app/domain"
	"bitbucket.org/youplus/trackinginfoservice/app/domain/vm"
	"github.com/sirupsen/logrus"
	"github.com/streadway/amqp"

	"bitbucket.org/youplus/trackinginfoservice/app/infrastructure"
	"bitbucket.org/youplus/trackinginfoservice/app/infrastructure/engage"
	"bitbucket.org/youplus/trackinginfoservice/app/utils"
	"gopkg.in/mgo.v2/bson"
)

//EngagementService ..
type EngagementService struct{}

var _dbConfig = infrastructure.ConfigService{}
var _dbInstance = infrastructure.DB{}
var _dbEngageInstance = engage.DB{}
var _logger = utils.GetLogger()

var rmqService = infrastructure.QueueService{}
var batchCurrentSize int
var batchSize int = 990

var widgetConfigColBulk = _dbInstance.GetMongo().C("widget_configs").Bulk()

//SaveTrackingInfo ... Save all the tracking info of a page

func (es *EngagementService) SaveTrackingInfo(data string) (map[string]interface{}, int) {
	var response map[string]interface{}
	var status int
	var paramsValid = true
	var trackedDataVM vm.UserEngagementVM
	var trackingData domain.UserEngagement
	var invalidParams []string

	err := json.Unmarshal(([]byte)(data), &trackedDataVM)

	var widgetStatusValues = []string{"live", "ready_to_launch", "queued", "preparing", "paused", ""}

	widgetStatusValid := utils.ContainsString(trackedDataVM.WidgetStatus, widgetStatusValues)

	if trackedDataVM.UniqueUserID == "" {
		_logger.Error("Bad uuid.")
		invalidParams = append(invalidParams, "uuid")
		paramsValid = false
	}

	if trackedDataVM.SessionID == "" {
		_logger.Error("Bad sid.")
		invalidParams = append(invalidParams, "sid")
		paramsValid = false
	}

	if trackedDataVM.StartTimestamp == nil {
		_logger.Error("Bad start_ts.")
		invalidParams = append(invalidParams, "start_ts")
		paramsValid = false
	}

	if domain.GetEventName(trackedDataVM.EventName) == domain.EVENTNAMEEND {
		_logger.Error("Bad event_name.")
		invalidParams = append(invalidParams, "event_name")
		paramsValid = false
	}

	if trackedDataVM.Label != "" && domain.GetLabel(trackedDataVM.Label) == domain.LABELEND {
		_logger.Error("Bad label.")
		invalidParams = append(invalidParams, "label")
		paramsValid = false
	}

	if trackedDataVM.VideoType != "content" && trackedDataVM.VideoType != "ad" && trackedDataVM.VideoType != "" {
		_logger.Error("Bad video type.")
		invalidParams = append(invalidParams, "video_type")
		paramsValid = false
	}

	widgetURL, urlErr := url.ParseRequestURI(trackedDataVM.WidgetURL)
	if trackedDataVM.WidgetURL != "" && urlErr != nil {
		_logger.Error("Bad widget url.")
		invalidParams = append(invalidParams, "widget_url")
		paramsValid = false
	}

	if err == nil && paramsValid && widgetStatusValid {
		database := _dbInstance.GetMongo()
		if database != nil {
			config := _dbConfig.GetConfiguration()
			var currentTime = time.Now()
			trackingData.ID = bson.NewObjectId()
			trackingData.UniqueUserID = trackedDataVM.UniqueUserID
			trackingData.SessionID = trackedDataVM.SessionID
			trackingData.GroupID = trackedDataVM.GroupID
			trackingData.WidgetID = trackedDataVM.WidgetID
			trackingData.WidgetURL = strings.Split(trackedDataVM.WidgetURL, "#")[0]
			trackingData.EventID = trackedDataVM.EventName
			trackingData.EventName = trackedDataVM.EventName
			trackingData.QuestionID = trackedDataVM.QuestionID
			trackingData.VideoID = trackedDataVM.VideoID
			trackingData.WidgetStatus = trackedDataVM.WidgetStatus
			trackingData.NewSession = trackedDataVM.NewSession
			trackingData.IsMobile = trackedDataVM.IsMobile

			// This block updates the sessions for the pre/post analytics
			// TODO : Push the post analytics data to queue
			if trackingData.NewSession && trackingData.WidgetStatus == "preparing" {
				go func() {
					mi := infrastructure.MongoInfra{}
					m := make(map[string]interface{})
					var wc domain.WidgetConfigs

					//TODO : Put the widget_configs in const or config file
					collection := database.C("widget_configs")
					err := collection.FindId(bson.ObjectIdHex(trackingData.WidgetID)).One(&wc)

					if err != nil {
						_logger.Error("failed to find the sessions : " + err.Error())
					}

					s := wc.Sessions

					if s == 0 {
						m["initial_session"] = currentTime.UTC()
					}

					// t1 := wc.InitialSession.UTC()
					// t2 := time.Now().UTC()
					// diff := t2.Sub(t1)

					// // TODO : Make these values configurable
					// if s != 0 && (s >= 1 || (diff.Seconds() >= 1 || diff.Hours()/24 >= 1)) {
					m["view_status"] = "live"
					// }

					s = s + 1
					m["sessions"] = s

					mi.UpdateWidgetConfigByID(bson.ObjectIdHex(trackingData.WidgetID), m)
				}()
			}

			if trackedDataVM.WidgetURL != "" {
				trackingData.Domain = widgetURL.Host
			}

			if trackedDataVM.StartTimestamp != nil {
				trackingData.StartTime = *trackedDataVM.StartTimestamp
			}

			if trackedDataVM.VideoType == "" {
				trackingData.VideoType = "content"
			} else {
				trackingData.VideoType = trackedDataVM.VideoType
			}

			if utils.ContainsString(trackedDataVM.EventName, []string{"CLICK", "AUTOPLAY", "MANUAL"}) {
				trackingData.Label = trackedDataVM.Label
			}

			trackingData.UserAgent = trackedDataVM.UserAgent
			trackingData.CreatedAt = currentTime.UTC()
			trackingData.UpdatedAt = currentTime.UTC()
			trackingData.ServerTimestamp = strconv.FormatInt(currentTime.Unix(), 10)

			s, _ := json.Marshal(trackingData)

			if rmqService.Publish(config.RMQHost, "track.new.queue", "track.new.exchange", "topic", s) {
				if bson.IsObjectIdHex(trackedDataVM.VideoID) && trackedDataVM.Label == "VIDEOPLAY" {
					go func() {
						qtagsCollection := database.C("youplus_q_tags")
						if qtagsCollection != nil {
							qterr := qtagsCollection.Update(bson.M{"_id": bson.ObjectIdHex(trackedDataVM.VideoID)}, bson.M{"$inc": bson.M{"view_count": 1}})
							if qterr != nil {
								_logger.Error("qtags collection is not updated : " + qterr.Error())
							}
						}
					}()
				}
				response = utils.GetErrResponse(utils.MSG_UPDATE_SUCCESS, []string{})
				status = http.StatusOK
			} else {
				_logger.Error(err.Error())
				response = utils.ErrorInternalServer{}.GetErrResponse()
				status = http.StatusInternalServerError
			}

		} else {
			response = utils.ErrorInternalServer{}.GetErrResponse()
			status = http.StatusInternalServerError
		}
	} else if err != nil {
		_logger.Error(err.Error())
		response = utils.GetErrResponse(utils.MSG_BAD_PARAMETERS, []string{})
		status = http.StatusBadRequest
	} else if !widgetStatusValid {
		_logger.Error("Bad widget_status")
		response = utils.ErrorOneof{}.GetErrResponse("widget_status", widgetStatusValues)
		status = http.StatusBadRequest
	} else {
		_logger.Error("Bad parameters received")
		response = utils.GetErrResponse(utils.MSG_BAD_VALUE, invalidParams)
		status = http.StatusBadRequest
	}
	if status != http.StatusOK {
		_logger.Error("Request data:", data)
	}
	return response, status
}

//ConsumeStatisticsFromRMQ ...
func (es *EngagementService) ConsumeStatisticsFromRMQ() {
	logrus.Info("ConsumeStatisticsFromRMQ : ", time.Now())

	var conn *amqp.Connection
	var channel *amqp.Channel
	var messages <-chan amqp.Delivery
	var batchData []interface{}
	var msg amqp.Delivery
	var err error

	if database := _dbEngageInstance.GetMongo(); database != nil {

		config := _dbConfig.GetConfiguration()
		engagementColl := database.C(config.YouplusEngagementColl)
		if engagementColl != nil {
			conn, err = amqp.Dial(config.RMQHost)
			if err == nil {
				defer conn.Close()
				channel, err = conn.Channel()
				if err == nil {
					defer channel.Close()
					_, err := channel.QueueDeclare("track.new.queue", true, false, false, false, nil)
					if err == nil {
						messages, err = channel.Consume("track.new.queue", "track.new.consumer", false, false, false, false, nil)
						if err == nil {
							defer channel.Cancel("track.new.consumer", false)
							ctx, cancel := context.WithTimeout(context.Background(), 1*time.Minute)
							defer cancel()
							for {
								select {
								case msg = <-messages:
									var data domain.UserEngagement
									err := json.Unmarshal(msg.Body, &data)

									if err == nil {

										if data.WidgetID != "" && bson.IsObjectIdHex(data.WidgetID) && data.EventName == "WIDGETLOAD" {
											widgetConfigColBulk.Update(bson.M{"_id": bson.ObjectIdHex(data.WidgetID)}, bson.M{"$inc": bson.M{"widget_loads": 1}})

											batchCurrentSize = batchCurrentSize + 1

											if batchCurrentSize == batchSize {
												_, bulkError := widgetConfigColBulk.Run()
												if bulkError != nil {
													_logger.Error("Error in bulk update:", bulkError.Error())
												}
												batchCurrentSize = 0

												widgetConfigColBulk = _dbInstance.GetMongo().C("widget_configs").Bulk()
											}
										}
										batchData = append(batchData, data)
										if len(batchData) == batchSize {
											_logger.Info("---Writing to DB length of queue---", len(batchData))
											start := time.Now()

											err := engagementColl.Insert(batchData...)

											elapsed := time.Since(start)
											_logger.Info("Insert operation took (seconds): ", elapsed)
											if err == nil {
												batchData = batchData[:0]
												msg.Ack(true)
											} else {
												_logger.Error(err)
											}
										}
									} else {
										_logger.Error(err)
									}
								case <-ctx.Done():
									if len(batchData) > 0 {
										_logger.Info("---Writing to DB length of queue---", len(batchData))
										err := engagementColl.Insert(batchData...)

										_, bulkError := widgetConfigColBulk.Run()
										if bulkError != nil {
											_logger.Error("Error in bulk update:", bulkError.Error())
										}

										widgetConfigColBulk = _dbInstance.GetMongo().C("widget_configs").Bulk()

										batchCurrentSize = 0

										if err == nil {
											batchData = batchData[:0]
											msg.Ack(true)
										} else {
											_logger.Error(err)
										}
										msg.Ack(true)
									}
									_logger.Info("exiting consumer at: ", time.Now())
									return
								}
							}
						}
					}
				}
			}
		}
	} else {
		err = errors.New("connection to db failed")
	}
	logrus.Error(err)
}
