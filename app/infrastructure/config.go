package infrastructure

import (
	"sync"

	"bitbucket.org/youplus/trackinginfoservice/app/utils"
	"github.com/tkanos/gonfig"
)

//ConfigService : Utility layer to incorporate all the helper function
type ConfigService struct{}

var _logger = utils.GetLogger()
var _configuration *Configuration
var once sync.Once

//Configuration : Database configuration
type Configuration struct {
	MongoDBHost               string `json:"mongodbhost"`
	MongoDBDatabase           string `json:"mongodbdatabase"`
	MongoDBUsername           string `json:"mongodbusername"`
	MongoDBPassword           string `json:"mongodbpassword"`
	YouplusQTagsColl          string `json:"youplusqtagscoll"`
	YouplusUserColl           string `json:"youplususercoll"`
	YouplusGroupColl          string `json:"youplusgroupcoll"`
	YouplusWidgetColl         string `json:"youpluswidgetcoll"`
	CrawledInfoColl           string `json:"crawledInfoColl"`
	YouplusEngagementColl     string `json:"youplusengagementcoll"`
	AdCampaignColl            string `json:"adcampaigncoll"`
	YouplusQVMColl            string `json:"youplusqvmcoll"`
	YouplusOpFeedbackColl     string `json:"youplusopfeedbackcoll"`
	StaticPageColl            string `json:"youplusstaticpagecoll"`
	ElasticHost               string `json:"elastic_host"`
	IosCertificateFile        string `json:"ios_cert_file"`
	IosPassword               string `json:"ios_password"`
	IsIosDev                  bool   `json:"ios_is_dev"`
	VicharNotificationTitle   string `json:"vichar_notification_title"`
	VicharNotificationBody    string `json:"vichar_notification_body"`
	Sender                    string `json:"sender"`
	Sub                       string `json:"sub"`
	Body                      string `json:"body"`
	TextBody                  string `json:"text_body"`
	CharSet                   string `json:"char_set"`
	MSG91Host                 string `json:"msg91Host"`
	SendOTP                   string `json:"msg91SendOtpApi"`
	VerifyOTP                 string `json:"msg91VerifyOtpApi"`
	AuthKey                   string `json:"msg91AuthKey"`
	MSG91Sender               string `json:"msg91Sender"`
	OTPLength                 string `json:"msg91OtpLength"`
	OTPExpire                 string `json:"msg91OtpExpire"`
	AccessKey                 string `json:"accesskey"`
	SecretKey                 string `json:"secretkey"`
	Region                    string `json:"region"`
	LogLevel                  string `json:"log_level"`
	AwsID                     string `json:"aws_id"`
	AwsSecret                 string `json:"aws_secret"`
	AwsRegion                 string `json:"aws_region"`
	AwsBucketNameForVideo     string `json:"aws_bucket_name_for_video"`
	AwsBucketNameForThumbnail string `json:"aws_bucket_name_for_thumbnail"`
	AwsCloudfrontURL          string `json:"aws_cloudfront_url"`
	GoAPIHost                 string `json:"goAPIHost"`
	IrEndPoint                string `json:"irEndPoint"`
	RMQHost                   string `json:"rmq_host"`
	EngagementMongoDBDatabase string `json:"EngagementMongoDBDatabase"`
	EngagementMongoDBHost     string `json:"EngagementMongoDBHost"`
	EngagementMongoDBUsername string `json:"EngagementMongoDBUsername"`
	EngagementMongoDBPassword string `json:"EngagementMongoDBPassword"`
}

//GetConfiguration ... Gets the instance for the logger.
func (cs *ConfigService) GetConfiguration() *Configuration {
	once.Do(func() {
		_configuration = &Configuration{}
		var err = gonfig.GetConf("./config.json", _configuration)
		if err != nil {
			_logger.Error("Error access configuration file: " + err.Error())
		}
	})
	return _configuration
}
