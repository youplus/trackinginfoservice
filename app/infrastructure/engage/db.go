package engage

import (
	"sync"
	"time"

	"bitbucket.org/youplus/trackinginfoservice/app/infrastructure"
	"bitbucket.org/youplus/trackinginfoservice/app/utils"
	"go.mongodb.org/mongo-driver/mongo"
	"gopkg.in/mgo.v2"
)

var _initMongoCtx sync.Once
var _session *mgo.Session

var _initMongoDBCtx sync.Once
var _mongoClient *mongo.Client

var _dbConfig = infrastructure.ConfigService{}

var _logger = utils.GetLogger()

//Database configuration constants
const (
	Debug   = false
	LocalDB = "test1"
)

//MongoDB ...
type MongoDB struct {
	MongoDatabase   *mgo.Database
	MongoDBDatabase *mongo.Database
}

var _mongoInstance *MongoDB

//DB  Infrastructure layer to incorporate all the database function
type DB struct{}

//GetMongo ...
func (ds *DB) GetMongo() *mgo.Database {
	_initMongoCtx.Do(func() {
		_logger.Info("Connecting to Mongo.....")
		var err error
		if !Debug {
			_config := _dbConfig.GetConfiguration()
			info := &mgo.DialInfo{
				Addrs:    []string{_config.EngagementMongoDBHost},
				Timeout:  60 * time.Second,
				Database: _config.EngagementMongoDBDatabase,
				Username: _config.EngagementMongoDBUsername,
				Password: _config.EngagementMongoDBPassword,
			}

			_session, err = mgo.DialWithInfo(info)
			_session.SetMode(mgo.Monotonic, true)
			if _mongoClient == nil {
				_mongoInstance = &MongoDB{MongoDatabase: _session.DB(_config.EngagementMongoDBDatabase)}
			} else {
				_mongoInstance = &MongoDB{
					MongoDBDatabase: _mongoClient.Database(_config.EngagementMongoDBDatabase),
					MongoDatabase:   _session.DB(_config.EngagementMongoDBDatabase)}
			}
		} else {
			_session, err = mgo.Dial("mongodb://localhost:27017")
			_mongoInstance = &MongoDB{MongoDatabase: _session.DB(LocalDB)}
		}
		if err != nil {
			_logger.Error("Failed to connect to Mongo..." + " " + err.Error())
			return
		}
		_logger.Info("Connected to Mongo...")
	})
	if !Debug {
		_session.Refresh()
	}
	//test cm=ommti
	return _mongoInstance.MongoDatabase
}
