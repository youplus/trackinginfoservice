package infrastructure

import (
	"errors"
	"log"
	"sync"
	"time"

	"github.com/sirupsen/logrus"
	"github.com/streadway/amqp"
)

//QueueService ...
type QueueService struct{}

//AMQPInstance ...
type AMQPInstance struct {
	Channel    *amqp.Channel
	Connection *amqp.Connection
	ErrChannel *chan *amqp.Error
}

var _amqpCtx sync.Once
var _amqpInstance *AMQPInstance

//GetInstance .. To connect with respective queue factory
func (qs *QueueService) GetInstance(connStr string) *AMQPInstance {
	if _amqpInstance == nil {
		qs.Connect(connStr)
	}
	return _amqpInstance
}

//Connect ... Connect to the rmq connection
func (qs *QueueService) Connect(connStr string) {
	var ec = make(chan *amqp.Error)
	go func() {
		err := <-ec
		time.Sleep(5 * time.Second)
		log.Println("reconnect: " + err.Error())
		qs.Connect(connStr)
	}()

	logrus.Info("connection string : ", connStr)
	conn, err := amqp.Dial(connStr)
	if err != nil {
		logrus.Error(err)
		ec <- &amqp.Error{Reason: err.Error()}
		return
	}
	logrus.Info("successfully connected to rmq ...")
	_amqpInstance = &AMQPInstance{Connection: conn, ErrChannel: &ec}
	conn.NotifyClose(ec)
	qs.registerConfiguration("track.new.exchange", "topic", "track.new.queue", true, false, false, false, nil)
}

func (qs *QueueService) registerConfiguration(exchangeName, exchangeType, queueName string, durable, autoDelete, internal, noWait bool, args amqp.Table) {
	var err error
	if _amqpInstance != nil {
		_amqpInstance.Channel, err = _amqpInstance.Connection.Channel()
		if err == nil {
			err = _amqpInstance.Channel.Qos(1, 0, false)
			if err == nil {
				logrus.Info("successfully set up QOS")
				err = _amqpInstance.Channel.ExchangeDeclare(exchangeName, exchangeType, durable, autoDelete, internal, noWait, args)
				if err == nil {
					logrus.Info("successfully declared the exchange : " + exchangeName)
					_, err = _amqpInstance.Channel.QueueDeclare(queueName, durable, autoDelete, internal, noWait, args)
					if err == nil {
						logrus.Info("successfully declared the queue : " + queueName)
						err = _amqpInstance.Channel.QueueBind(queueName, "#"+queueName, exchangeName, noWait, args)
						if err == nil {
							logrus.Info("successfully bound queue : ")
							logrus.Info("Queue : ", queueName)
							logrus.Info("Key : ", "#"+queueName)
							logrus.Info("Exchange : ", exchangeName)
						}
					}
				}
			}
		}
	} else {
		logrus.Error("failed to create instance of rabbit mq")
	}
	if err != nil {
		logrus.Error(err)
	}
}

//Publish ...
func (qs *QueueService) Publish(connStr, queueName, exchangeName, exchangeType string, payload []byte) bool {
	var err error

	_instance := qs.GetInstance(connStr)
	if _instance != nil {
		if err == nil {
			err = _instance.Channel.Publish(exchangeName, "#"+queueName, false, false, amqp.Publishing{
				DeliveryMode:    amqp.Persistent,
				Body:            payload,
				Timestamp:       time.Now(),
				ContentEncoding: "gzip",
				ContentType:     "application/json",
			})
			if err == nil {
				return true
			}
			log.Println(err.Error())
		}
	} else {
		err = errors.New("rabbit mq instance null")
	}
	if err != nil {
		log.Println(err.Error())
	}
	return false
}
