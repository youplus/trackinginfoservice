package infrastructure

import (
	"gopkg.in/mgo.v2/bson"
)

//MongoInfra : Infrastructure layer to incorporate all the mongo CRUD
type MongoInfra struct{}

var _db = DB{}

//UpdateWidgetConfigByID updates the widget config matching the ID
func (ma *MongoInfra) UpdateWidgetConfigByID(id bson.ObjectId, params map[string]interface{}) bool {
	var _instance = _db.GetMongo()
	if len(params) > 0 {
		match := bson.M{"_id": id}
		fields := bson.M{}

		for i, param := range params {
			fields[i] = param
		}

		if _instance != nil {
			// TODO : Add widget_configs to const or to the config
			collection := _instance.C("widget_configs")

			err := collection.Update(match, bson.M{"$set": fields})
			if err != nil {
				_logger.Error("Failed to update the widget config" + err.Error())
				return false
			}
		}
		return true
	}
	return false
}
